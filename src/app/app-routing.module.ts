import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';


const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'array'},
  {
    path: 'array',
    loadChildren: () => import('./modules/array-page/array-page.module').then(m => m.ArrayPageModule)
  },
  {
    path: 'dict',
    loadChildren: () => import('./modules/dict-page/dict-page.module').then(m => m.DictPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
