import {Component, OnInit} from '@angular/core';
import {NavService} from '@core/services/nav.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Postulación Front';

  constructor(public navService: NavService) {
  }

  ngOnInit() {
    this.navService.setNav([
      {
        link: '/array',
        label: 'Array'
      },
      {
        link: '/dict',
        label: 'Dict'
      },
    ])
  }

}
