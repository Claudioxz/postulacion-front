import {Injectable} from '@angular/core';
import {catchError, map} from 'rxjs/internal/operators';
import {Observable, throwError} from "rxjs";
import {ApiHttpService} from '@core/http/api-http.service';

@Injectable({
  providedIn: 'root'
})
export class DictService {


  constructor(private apiHttp: ApiHttpService) {
  }

  public getDictData(): Observable<ProcessParagraph[]> {
    return this.apiHttp.getDict().pipe(
      map(data => {

        if (!data.success || !data.data) {
          throw new Error(data.error);
        }

        const parsedData = JSON.parse(data.data) as Paragraph[];
        return this.countLettersAndSumNumbers(parsedData);
      }),
      catchError(error => throwError(error))
    );
  }

  private countLettersAndSumNumbers(paragraphs: Paragraph[]): ProcessParagraph[] {
    return paragraphs.map(item => {

      const letterCountMap = this.countLettersInText(item.paragraph);
      const totalNumbers = this.sumNumbersInText(item.paragraph);

      return {
        letterCountMap,
        totalNumbers,
        text: item.paragraph
      }
    });
  }

  /**
   * Cuenta la cantidad de ocurrencias por cada letra del alfabeto
   * @param text Texto a procesar
   * @return Retorna un map de un largo de 27 con las letras como key y ocurrencias como value
   */
  private countLettersInText(text: string): Map<string, number> {
    const alphabetMap = this.getNewAlphabetMap();
    for (let i = 0; i < text.length; i++) {
      const currChar = text.charAt(i).toLowerCase();

      //Si el carácter existe en el map suma una ocurrencia
      if (alphabetMap.has(currChar)) {
        const currCount = alphabetMap.get(currChar);
        alphabetMap.set(currChar, currCount + 1);
      }
    }

    return alphabetMap;
  }

  /**
   * Suma el total de números encontrados en un texto
   * @param text Texto a procesar
   * @return Retorna el total de números
   */
  private sumNumbersInText(text: string): number {

    let totalNumbers = 0;

    for (let i = 0; i < text.length; i++) {

      const currChar = text.charAt(i);
      if (this.checkIfNotNumber(currChar)) {
        continue; //Saltar si el carácter no es un numero
      }

      let completeNumber = currChar;
      let skip = 0;

      //Verificar si los siguientes caracteres son números
      while (true) {
        const nextChar = text.charAt(i + skip + 1);
        if (this.checkIfNotNumber(nextChar)) {
          break;
        }
        //Si es un numero concatenar
        completeNumber += nextChar;
        skip++;
      }
      //Omitir caracteres en el for por la cantidad de números que fueron encontrados en el while
      i += skip;

      totalNumbers += parseInt(completeNumber, 10);
    }
    return totalNumbers;
  }


  private getNewAlphabetMap(): Map<string, number> {
    return new Map<string, number>(
      ALPHABET.map(letter => [letter, 0])
    );
  }

  private checkIfNotNumber(s: string) {
    return isNaN(parseInt(s, 10));
  }
}


export const ALPHABET = 'abcdefghijklmnñopqrstuvwxyz'.split('');

export interface ProcessParagraph {
  letterCountMap: Map<string, number>;
  totalNumbers: number;
  text: string;
}

export interface Paragraph {
  hasCopyright: boolean;
  paragraph: string;
  number: number;
}
