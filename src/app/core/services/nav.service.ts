import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavService {

  private navSubject = new BehaviorSubject<NavItem[]>([]);

  public nav$ = this.navSubject.asObservable();

  constructor() {
  }

  public setNav(navItems: NavItem[]) {
    this.navSubject.next(navItems);
  }

}

export interface NavItem {
  label: string;
  link: string;
}
