import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/internal/operators';
import {ApiHttpService} from '@core/http/api-http.service';

@Injectable({
  providedIn: 'root'
})
export class ArrayService {

  constructor(private apiHttp: ApiHttpService) {
  }

  /**
   * Está función obtiene los datos del servicio ApiHttp
   * y los mapea con los datos deseados del componente
   */
  public getArray(): Observable<ArrayData> {
    return this.apiHttp.getArray().pipe(
      map(data => {
        if (!data.success || !data.data) {
          throw new Error(data.error);
        }

        const items = this.mapNumbersToArrayData(data.data);
        const orderedNumbers = this.orderNumbers(data.data);
        return {
          items,
          orderedNumbers
        };
      }),
      catchError(e => throwError(e)) //Si existe un error en la api delegar este
    );
  }

  /**
   * Esta función mapea un array de números a un array de tipo ArrayItem
   * @param numberList Una lista de números
   */
  private mapNumbersToArrayData(numberList: number[]): ArrayItem[] {
    const array: ArrayItem[] = [];

    for (let number of numberList) {

      const index = array.findIndex(item => item.number === number);
      const exists = index >= 0;
      if (exists) {
        continue; //Si el numero ya existe seguir con la siguiente iteración
      }

      //Obtiene la cantidad de ocurrencias del numero actual
      const quantity = numberList.filter(item => item === number).length;

      const firstPosition = numberList.indexOf(number);
      const lastPosition = numberList.lastIndexOf(number);

      array.push({
        number, quantity, firstPosition, lastPosition
      });
    }

    return array;
  }

  /**
   * Ordena una lista de números de menor a mayor
   * @param numberList
   */
  private orderNumbers(numberList: number[]): number[] {

    const orderedArray = [];
    const copyList = [...numberList];

    for (let i = 0; i < numberList.length; i++) {

      //Obtiene el mínimo valor de la lista
      const minValue = Math.min(...copyList);

      //Elimina el mínimo valor de la lista
      const minValueIndex = copyList.findIndex(number => number === minValue);
      copyList.splice(minValueIndex, 1);

      //Agrega el mínimo valor a la lista ordenada
      orderedArray.push(minValue);

    }
    return orderedArray;
  }

}

export const EMPTY_ARRAY_DATA: ArrayData = {
  items: [],
  orderedNumbers: []
};

export interface ArrayData {
  items: ArrayItem[];
  orderedNumbers: number[];
}

export interface ArrayItem {
  number: number;
  quantity: number;
  firstPosition: number;
  lastPosition: number;
}
