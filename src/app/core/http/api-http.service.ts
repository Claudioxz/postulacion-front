import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '@environment/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiHttpService {

  constructor(private http: HttpClient) {
  }

  public getArray(): Observable<ArrayApiData> {
    return this.http.get<ArrayApiData>(`${environment.apiUrl}/array.php`);
  }

  public getDict(): Observable<ApiData<string>> {
    return this.http.get<ApiData<string>>(`${environment.apiUrl}/dict.php`);
  }
}


export type ArrayApiData = ApiData<number[]>;

export interface ApiData<T> {
  data?: T | null;
  error: any;
  success: boolean;
}
