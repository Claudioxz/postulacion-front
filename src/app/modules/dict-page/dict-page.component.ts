import {Component, OnInit} from '@angular/core';
import {ALPHABET, DictService, ProcessParagraph} from "../../core/services/dict.service";
import {EMPTY, Observable, of, Subject} from "rxjs";
import {catchError, repeat, switchMap, tap} from "rxjs/operators";

@Component({
  selector: 'app-dict-page',
  templateUrl: './dict-page.component.html',
  styleUrls: ['./dict-page.component.scss']
})
export class DictPageComponent implements OnInit {

  private loadData = new Subject<void>();

  public processedParagraph$: Observable<ProcessParagraph[]>;

  public loading = false;

  public error = null;


  constructor(private dictService: DictService) {
  }

  ngOnInit() {
    this.processedParagraph$ = this.loadData.pipe(
      tap(() => {
        this.error = null;
        this.loading = true;
      }),
      switchMap(() => this.dictService.getDictData()),
      tap(() => this.loading = false),

      //Manejo de errores
      catchError(error => {
        this.error = 'Hubo un error, por favor inténtelo nuevamente';
        if (typeof error === 'string') {
          this.error = error;
        }
        this.loading = false;
        return of([]);
      }),
      repeat() //Si hubo un error continuar emitiendo valores
    );
  }

  getData() {
    this.loadData.next();
  }


}
