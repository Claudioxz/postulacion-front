import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DictPageComponent} from './dict-page.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import { DictTableComponent } from './components/dict-table/dict-table.component';

const routes: Routes = [
  {path: '', component: DictPageComponent}
]

@NgModule({
  declarations: [DictPageComponent, DictTableComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class DictPageModule {
}
