import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {ALPHABET, ProcessParagraph} from '@core/services/dict.service';

@Component({
  selector: 'app-dict-table',
  templateUrl: './dict-table.component.html',
  styleUrls: ['./dict-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DictTableComponent implements OnChanges {

  @Input() loading: boolean;
  @Input() paragraphs: ProcessParagraph[];

  public header = ['N°', ...ALPHABET, 'Suma numéricos contenidos'];
  public paragraphsCounts: ParagraphCount[] = [];

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.paragraphsCounts = this.paragraphs.map((p) => ({
      letterCount: Array.from(p.letterCountMap.values()),
      totalNumbers: p.totalNumbers
    }));
  }
}

interface ParagraphCount {
  letterCount: number[];
  totalNumbers: number;
}
