import {Component, Input, OnInit} from '@angular/core';
import {NavItem} from '@core/services/nav.service';

@Component({
  selector: 'app-nav[navItems]',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  @Input() navItems: NavItem[];
  @Input() title: string;

  constructor() { }

  ngOnInit() {
  }

}
