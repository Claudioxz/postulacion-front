import {Component, OnInit} from '@angular/core';
import {Observable, of, Subject} from 'rxjs';
import {catchError, repeat, switchMap, tap} from 'rxjs/internal/operators';
import {ArrayData, ArrayService, EMPTY_ARRAY_DATA} from '@core/services/array.service';

@Component({
  selector: 'app-array-page',
  templateUrl: './array-page.component.html',
  styleUrls: ['./array-page.component.scss']
})
export class ArrayPageComponent implements OnInit {

  public arrayData$: Observable<ArrayData>;
  public loading = false;
  public error = null;

  private loadData = new Subject<void>();

  constructor(private arrayService: ArrayService) {
  }

  ngOnInit() {
    this.arrayData$ = this.loadData.pipe(
      tap(() => {
        this.error = null;
        this.loading = true
      }),
      switchMap(() =>
        this.arrayService.getArray()
      ),
      tap(() => this.loading = false),
      catchError(error => {
        this.error = 'Hubo un error, por favor inténtelo nuevamente';
        if (typeof error === 'string') {
          this.error = error;
        }
        this.loading = false;
        return of(EMPTY_ARRAY_DATA);
      }),
      repeat()
    );
  }

  getData() {
    this.loadData.next();
  }
}
