import {Component, Input, OnInit} from '@angular/core';
import {ArrayItem} from '@core/services/array.service';

@Component({
  selector: 'app-array-table[items]',
  templateUrl: './array-table.component.html',
  styleUrls: ['./array-table.component.scss']
})
export class ArrayTableComponent implements OnInit {

  @Input() items: ArrayItem[];
  @Input() loading: boolean;

  readonly header = ['N°', 'number', 'quantity', 'first position', 'last position'];

  constructor() { }

  ngOnInit() {
  }

}
