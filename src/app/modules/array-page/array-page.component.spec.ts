import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArrayPageComponent } from './array-page.component';

describe('ArrayPageComponent', () => {
  let component: ArrayPageComponent;
  let fixture: ComponentFixture<ArrayPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArrayPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArrayPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
