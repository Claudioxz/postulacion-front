import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '@shared/shared.module';
import {ArrayPageComponent} from './array-page.component';
import {ArrayTableComponent} from './components/array-table/array-table.component';

const routes: Routes = [
  {path: '', component: ArrayPageComponent}
]

@NgModule({
  declarations: [
    ArrayPageComponent,
    ArrayTableComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class ArrayPageModule {
}
