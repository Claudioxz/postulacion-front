import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertComponent } from './components/alert/alert.component';
import { TableComponent } from './components/table/table.component';
import { ButtonComponent } from './components/button/button.component';



@NgModule({
  declarations: [
    AlertComponent,
    TableComponent,
    ButtonComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    AlertComponent,
    TableComponent,
    ButtonComponent
  ]
})
export class SharedModule { }
