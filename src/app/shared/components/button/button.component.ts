import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-button[text]',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input() loading: boolean;
  @Input() text: string;

  constructor() { }

  ngOnInit() {
  }

}
