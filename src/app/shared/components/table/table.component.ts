import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-table[header]',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  @Input() header: string[];
  @Input() loading: boolean = false;
  @Input() tableClass = '';

  constructor() { }

  ngOnInit() {
  }

}
