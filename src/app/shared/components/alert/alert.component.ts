import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-alert[type]',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  @Input() type: AlertType;

  @Input() show = false;

  constructor() {
  }

  ngOnInit() {
  }

  closeAlert(){
    this.show = false;
  }

}

export type AlertType = 'primary' | 'success' | 'secondary' | 'info' | 'danger';
